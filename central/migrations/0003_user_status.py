# Generated by Django 2.2.2 on 2019-06-07 00:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('central', '0002_spamm'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='status',
            field=models.CharField(choices=[(0, 'Закончен'), (3, 'Новый'), (2, 'Ошибка'), (1, 'В процессе')], default=3, max_length=30),
        ),
    ]
