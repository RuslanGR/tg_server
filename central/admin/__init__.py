from django.contrib import admin

from ..models import Category, User, Spamm
from .user import UserAdmin
from .spamm import SpammAdmin

admin.site.register(User, UserAdmin)
admin.site.register(Category)
admin.site.register(Spamm, SpammAdmin)
