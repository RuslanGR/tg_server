from django.contrib import admin
from django.utils.safestring import mark_safe

from ..models import User


class UserAdmin(admin.ModelAdmin):

    def status_field(self, obj):
        base_str = '<div style="background-color: {color}; padding: 0; margin: 0; width: 50px">{message}</div>'

        if obj.status == User.STATUS_ERROR_CHOICE:
            return mark_safe(base_str.format(color='red', message='error'))
        if obj.status == User.STATUS_DONE_CHOICE:
            return mark_safe("""
            <div style="background-color: green; padding: 0; margin: 0; width: 50px">done</div>
            """)
        if obj.status == User.STATUS_NEW_CHOICE:
            return mark_safe("""
            <div style="background-color: blue; padding: 0; margin: 0; width: 50px">new</div>
            """)
        if obj.status == User.STATUS_PROCESS_CHOICE:
            return mark_safe("""
            <div style="background-color: yellow; padding: 0; margin: 0; width: 50px">progress</div>
            """)
        else:
            return 'lol'

    def make_new(self, request, users):
        users.update(status=User.STATUS_NEW_CHOICE, spammed=False)
        self.message_user(request, 'Good job, David')

    make_new.short_description = 'Make them new again'

    search_fields = ('category__name',)
    list_display = ('id', 'user_id', 'spammed', 'category', 'status_field')
    list_filter = ('spammed', 'category')
    actions = (make_new,)
