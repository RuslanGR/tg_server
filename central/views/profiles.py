from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from ..models import User, Spamm
from ..serializers import UserSerializer


class ProfilesAPIView(APIView):
    """Work with users ids"""

    def get(self, _):
        """Get profile to spam"""
        process = Spamm.objects.first()
        users = User.objects.filter(spammed=False, status=User.STATUS_NEW_CHOICE, category=process.category)
        last_user = users.last()

        if not last_user:
            return Response({}, status=status.HTTP_502_BAD_GATEWAY)

        serialized_user = UserSerializer(last_user).data
        print(serialized_user)

        last_user.status = User.STATUS_PROCESS_CHOICE
        last_user.save()

        return Response(serialized_user)

    def post(self, request):
        """Mark profile as spammed"""
        data = request.POST

        try:
            user = User.objects.get(user_id=data.get('user_id'))
        except ObjectDoesNotExist:
            return Response({}, status=status.HTTP_502_BAD_GATEWAY)

        # User have uncompleted, return status to 'new'
        if data.get('new'):
            print(f'Spamming was declined, return {user.user_id} status to "New"')
            user.status = User.STATUS_NEW_CHOICE
            user.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        if data.get('no_image'):
            print(f'Spamming was declined, return {user.user_id} status to "Error"')
            user.status = User.STATUS_ERROR_CHOICE
            user.save()
            return Response(status=status.HTTP_202_ACCEPTED)

        print(f'Spammed: {user}' if data.get('spammed') else f'Not spammed: {user}')

        # Save new information
        user.status = User.STATUS_DONE_CHOICE
        if data.get('spammed'):
            user.spammed = True
        user.save()

        return Response(status=status.HTTP_202_ACCEPTED)
