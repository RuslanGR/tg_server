from django.core.management.base import BaseCommand

from central.models import User, Category
from central.parsers import ChatParser
from conf import settings


class Command(BaseCommand):
    """Custom command for parsing
    Activate: python manage.py chat_parse chat category
    """

    help = 'Parse chat members'
    API_ID = settings.API_ID
    API_HASH = settings.API_HASH
    PROXY_HOST = "94.189.132.47"
    PROXY_PORT = 8080

    def add_arguments(self, parser):
        parser.add_argument('chat', type=str, help='chat')
        parser.add_argument('category', type=str, help='category')

    def handle(self, *args, **kwargs):
        self.stdout.write(f'Category: {kwargs["category"]}\nChat: {kwargs["chat"]}')
        self.parse(self.API_ID, self.API_HASH, category_name=kwargs['category'], chat_name=kwargs['chat'],
                   proxy_host=self.PROXY_HOST, proxy_port=self.PROXY_PORT)
        self.stdout.write(kwargs['chat'])
        self.stdout.write('Done')

    @staticmethod
    def parse(api_id, api_hash, category_name, chat_name, proxy_host=None,
                    proxy_port=None, limit=None):

        if not category_name or not chat_name:
            return

        category, _ = Category.objects.get_or_create(name=category_name)

        def callback(username):
            print(username)
            if username:
                try:
                    User.objects.get_or_create(user_id=username, category=category)
                except Exception:
                    print('Error')
        parser = ChatParser(api_hash=api_hash, api_id=api_id, proxy_host=proxy_host,
                            proxy_port=proxy_port, db_name='enot02')
        parser.run(chat_name=chat_name, callback=callback, limit=limit)
