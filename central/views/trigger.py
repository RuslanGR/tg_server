import asyncio

from django.shortcuts import render
from django.views import View

from central.models import User, Category
from central.parsers import ChatParser


API_ID = 715154
API_HASH = '79f51656711bf4f9149e9922666cbb4a'


class TriggerView(View):
    """Central view"""

    def get(self, request):
        return render(request, 'index.html')

    def post(self, request):
        print('post')
        try:
            loop = asyncio.new_event_loop()
            loop.run_until_complete(self.parse(api_id=API_ID, api_hash=API_HASH))
            loop.close()
        except RuntimeError:
            print('Loop taken')

        return render(request, 'index.html')

    @staticmethod
    async def parse(api_id, api_hash, proxy_host=None, proxy_port=None, limit=None):
        category_name = 'test'
        chat_name = 'debily12'

        if not category_name or not chat_name:
            return

        category, _ = Category.objects.get_or_create(name=category_name)

        def callback(username):
            print(username)
            if username:
                User.objects.get_or_create(user_id=username, category=category)

        parser = ChatParser(api_hash=api_hash, api_id=api_id, proxy_host=proxy_host,
                            proxy_port=proxy_port, db_name='enot02')
        await parser.run(chat_name=chat_name, callback=callback, limit=limit)
