## SETUP ##

1. Create virtual environment and then load all
requirements from requirements.txt file.

2. Setup a database connection
    1. Copy .env-example as .env and fill all fields
3. Setup a database
    1. python manage.py migrate
    2. python manage.py createsuperuser
4. Run server
    1. python manage.py runserver


# Start parsing
```
python manage.py chat_parse <chat> <category>
```

# Convert txt to db
```
python manage.py upload --chat dzen2 --path C:\Users\ruslan\Downloads\ydzen.txt
```