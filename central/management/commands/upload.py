from django.core.management.base import BaseCommand
from django.db.utils import DataError

from central.models import User, Category


class Command(BaseCommand):
    """Custom command for parsing
    Activate: python manage.py upload path category
    """

    def add_arguments(self, parser):
        parser.add_argument('--chat', type=str, help='chat')
        parser.add_argument('--path', type=str, help='path')

    def handle(self, *args, **kwargs):
        category, _ = Category.objects.get_or_create(name=kwargs['chat'])

        with open(kwargs['path']) as f:
            for line in f.readlines():
                if line:
                    username = line.replace('@', '')
                    if username:
                        try:
                            User.objects.get_or_create(category=category, user_id=username)
                        except DataError:
                            print('Data error')

        self.stdout.write('Done')
