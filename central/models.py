from django.db import models


class Category(models.Model):
    """Модель таблицы Category"""

    name = models.CharField(max_length=240)

    objects = models.Manager()

    def __str__(self):
        return self.name


class User(models.Model):
    """Модель таблицы User"""

    STATUS_DONE_CHOICE = 'DONE'
    STATUS_PROCESS_CHOICE = 'PROCESS'
    STATUS_ERROR_CHOICE = 'ERROR'
    STATUS_NEW_CHOICE = 'NEW'
    STATUS_CHOICES = (
        (STATUS_DONE_CHOICE, 'Закончен'),
        (STATUS_NEW_CHOICE, 'Новый'),
        (STATUS_ERROR_CHOICE, 'Ошибка'),
        (STATUS_PROCESS_CHOICE, 'В процессе')
    )

    status = models.CharField(max_length=30, choices=STATUS_CHOICES, default=STATUS_NEW_CHOICE)
    user_id = models.CharField(max_length=100)
    spammed = models.BooleanField(default=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='users')

    objects = models.Manager()

    class Meta:

        unique_together = ("user_id", "category")

    def __str__(self):
        return f'{self.user_id} {self.spammed}'


class Spamm(models.Model):
    """Модель процесса"""

    text = models.TextField(verbose_name='Текст для спама')
    status = models.BooleanField(verbose_name='Программа запущена')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.status)
