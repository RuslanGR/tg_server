import asyncio

import socks
from telethon import TelegramClient


class ChatParser(object):
    """Chat participants parser"""

    def __init__(self, proxy_host, proxy_port, api_id, api_hash, db_name='name'):
        self.proxy_host = proxy_host
        self.proxy_port = proxy_port
        self.api_id = api_id
        self.api_hash = api_hash
        self.db_name = db_name

    async def parse(self, chat_name, callback, limit: int = None):
        """
        Parse chat and after calls callback to every id
        :param chat_name:
        :param callback:
        :param limit:
        :return:
        """

        if self.proxy_host and self.proxy_port:
            client = TelegramClient(self.db_name, self.api_id, self.api_hash,
                                    proxy=(socks.HTTP, self.proxy_host, self.proxy_port))
        else:
            client = TelegramClient(self.db_name, self.api_id, self.api_hash)

        async with client:

            print('So, waiting...')
            users = await client.get_participants(chat_name, limit=limit)
            for user in users:
                callback(user.username)

    def run(self, chat_name: str, callback, limit: int=None):
        """
        Run loop with requests
        :param chat_name:
        :param callback:
        :param limit:
        :return:
        """
        print('Run')
        asyncio.get_event_loop().run_until_complete(self.parse(chat_name=chat_name, callback=callback, limit=limit))
