from django.contrib import admin


class SpammAdmin(admin.ModelAdmin):

    def toggle(self, request, spamms):
        spamms.update(status=False)
        self.message_user(request, 'Good job, David')

    toggle.short_description = 'Toggle'

    list_display = ('status', 'category')
    actions = (toggle,)
