from django.http.response import JsonResponse

from ..models import Spamm


def get_status(_):
    process = Spamm.objects.first()
    return JsonResponse({'status': process.status})


def get_text(_):
    process = Spamm.objects.first()
    return JsonResponse({'text': process.text})
