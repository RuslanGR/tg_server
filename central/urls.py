from django.urls import path

from .views import TriggerView, get_status, ProfilesAPIView, get_text


urlpatterns = (
    path('', TriggerView.as_view()),
    path('status/', get_status),
    path('profiles/', ProfilesAPIView.as_view()),
    path('text/', get_text),
)
