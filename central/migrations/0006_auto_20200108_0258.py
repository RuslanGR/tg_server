# Generated by Django 2.2.2 on 2020-01-07 23:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('central', '0005_spamm_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(max_length=240),
        ),
    ]
